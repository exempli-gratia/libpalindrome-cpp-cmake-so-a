#include "palindrome.h"
#include <iostream>
#include <cstring>
#include <string>

bool isPalindrome(char* word)
{
    const std::string MY_STRING = "Hello from libpalindrome!";
    bool ret = true;

    char *p = word;
    int len = strlen(word);
    char *q = &word[len-1];

    for (int i = 0 ; i < len ; ++i, ++p, --q)
    {
        if (*p != *q)
        {
            ret = false;
        }
    }

    return ret;
}
